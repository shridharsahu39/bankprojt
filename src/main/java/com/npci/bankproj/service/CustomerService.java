package com.npci.bankproj.service;

import java.util.*;

import com.npci.bankproj.entity.Customer;

public interface CustomerService {

	public List<Customer> getAllCustomer();

	public Customer getCustomerById(int custId) throws Exception;

//	public Customer addOrUpdateCustomer(String custName, int custAge, String custAddress, long custAadhar,
//			String custPancard, String custEmail, long custPhone, char custSex, float accBalance);

	public Customer deleteCustomer(int custID) throws Exception;

	public Customer addOrUpdateCustomer(Customer customer);


}
