package com.npci.bankproj.transrepo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.RequestBody;

import com.npci.bankproj.transentity.Transaction;


public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	@Modifying
	@Query(value = " update customer set customer.acc_balance =(customer.acc_balance)-(:tAmount) where customer.cust_id=:toCustId; ",nativeQuery = true )
	public Transaction updateBalanceFromCustomer(@RequestBody Transaction transaction);
	
	@Modifying
	@Query(value = " update customer set customer.acc_balance =(customer.acc_balance)+(:tAmount) where customer.cust_id=:toCustId; ",nativeQuery = true )
	public Transaction updateBalanceToCustomer(@RequestBody Transaction transaction);
}
