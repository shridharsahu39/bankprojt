package com.npci.bankproj.custresponse;

import javax.persistence.Column;

import lombok.Data;

@Data
public class CustomerResponse {

	private int custId;

	private String custName;

	private int custAge;

	private String custAddress;

	private long custAadhar;

	private String custPancard;

	private String custEmail;

	private long custPhone;

	private long acc_number;

	private char custSex;

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public int getCustAge() {
		return custAge;
	}

	public void setCustAge(int custAge) {
		this.custAge = custAge;
	}

	public String getCustAddress() {
		return custAddress;
	}

	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}

	public long getCustAadhar() {
		return custAadhar;
	}

	public void setCustAadhar(long custAadhar) {
		this.custAadhar = custAadhar;
	}

	public String getCustPancard() {
		return custPancard;
	}

	public void setCustPancard(String custPancard) {
		this.custPancard = custPancard;
	}

	public String getCustEmail() {
		return custEmail;
	}

	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}

	public long getCustPhone() {
		return custPhone;
	}

	public void setCustPhone(long custPhone) {
		this.custPhone = custPhone;
	}

	public long getAcc_number() {
		return acc_number;
	}

	public void setAcc_number(long acc_number) {
		this.acc_number = acc_number;
	}

	public char getCustSex() {
		return custSex;
	}

	public void setCustSex(char custSex) {
		this.custSex = custSex;
	}

	public CustomerResponse(int custId, String custName, int custAge, String custAddress, long custAadhar,
			String custPancard, String custEmail, long custPhone, long acc_number, char custSex) {
		super();
		this.custId = custId;
		this.custName = custName;
		this.custAge = custAge;
		this.custAddress = custAddress;
		this.custAadhar = custAadhar;
		this.custPancard = custPancard;
		this.custEmail = custEmail;
		this.custPhone = custPhone;
		this.acc_number = acc_number;
		this.custSex = custSex;
	}

	public CustomerResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "CustomerResponse [custId=" + custId + ", custName=" + custName + ", custAge=" + custAge
				+ ", custAddress=" + custAddress + ", custAadhar=" + custAadhar + ", custPancard=" + custPancard
				+ ", custEmail=" + custEmail + ", custPhone=" + custPhone + ", acc_number=" + acc_number + ", custSex="
				+ custSex + "]";
	}

	
	
}
