package com.npci.bankproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BankProjtApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankProjtApplication.class, args);
	}

}
