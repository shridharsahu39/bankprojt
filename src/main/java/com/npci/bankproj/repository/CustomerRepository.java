package com.npci.bankproj.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.npci.bankproj.entity.Customer;


public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	@Query(value = "Insert into customer(cust_name, cust_age, cust_address, cust_aadhar, cust_pancard, cust_email, cust_phone,sex, acc_balance) values(?,?,?,?,?,?,?,?)", nativeQuery = true)
	public Customer addCustomer(String custName, int custAge, String custAddress, long custAadhar, String custPancard,
			String custEmail, long custPhone, char custSex, float accBalance);
}
