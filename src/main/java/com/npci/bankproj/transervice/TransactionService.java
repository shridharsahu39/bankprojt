package com.npci.bankproj.transervice;

import java.util.List;

import com.npci.bankproj.transrepo.*;
import com.npci.bankproj.transentity.Transaction;

public interface TransactionService {

	public List<Transaction> getAllTransaction();

	public Transaction getTransById(int serialId);

	public Transaction addTransaction(Transaction transaction) throws Exception;


}
